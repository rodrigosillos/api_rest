  <!-- Content -->
  <main id="main-content">

    <!-- Conteúdo Home -->
    <?php include "components/banner-slide.php"; ?>

    <div class="section-mask">
      <div class="container">
        <h2 class="section-mask-title"><svg class="icon icon-cesta"><use xlink:href="#icon-cesta"></use></svg> <span>Onde Comprar</span></h2>
      </div>
    </div>

    <div class="search-advanced">
      <div class="container">
        <div class="box-form-search-advanced">
          <input type="search" class="typeahead" placeholder="Digite o nome do produto">
        </div>
      </div>
    </div>


    <div class="section is-medium">
      <div class="container">
        <?php include 'components/box-slide.php'; ?>
      </div>
    </div>

    <div class="section-mask is-secondary">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12">
            <h2 class="section-mask-title">Saúde Feminina</h2>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="box-min-slide">
              <div class="slide-controls">
                <button class="btn-prev prev-item"></button>
                <button class="btn-next next-item"></button>
              </div>
              <div class="slide" slide>
                <div class="item">
                  <h3>Endometriose</h3>
                  <p>A endometriose é uma doença frequente, crônica...</p>
                </div>
                <div class="item">
                  <h3>Endometriose</h3>
                  <p>A endometriose é uma doença frequente, crônica...</p>
                </div>
                <div class="item">
                  <h3>Endometriose</h3>
                  <p>A endometriose é uma doença frequente, crônica...</p>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>



    <!-- / Conteúdo Home -->
  </main>