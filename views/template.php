<!DOCTYPE html>
<html lang="pt-BR" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Supera - Sinergia em Saúde</title>

  <link rel="stylesheet" href="assets/css/style.min.css">
</head>
<body>

  <!-- Header -->
  <?php echo $header; ?>


  <!-- Content -->
  <main id="main-content">
    <?php 
    /**
     * DEVELOPERS
     * Aqui vai o conteúdo da página
     *
     */
    echo $content;
    ?>
  </main>


  <!-- Footer -->
  <?php echo $footer; ?>

  
  <!-- Scripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" crossorigin="anonymous"></script>
  <script src="assets/js/vendor.min.js" crossorigin="anonymous"></script>
  <script src="assets/js/scripts.min.js" crossorigin="anonymous"></script>
  <!--<script src="assets/js/scripts/autocompleate.js" crossorigin="anonymous"></script>-->

  <script>
      $(function(){

          $('#btn-logout').click(function(){
              window.location.href = 'account/logout/';
          });

          $("form[name='form-login']").submit(function(e){

              e.preventDefault();

              $.ajax({
                  url: "account/login/",
                  type: 'post',
                  data: $("form[name='form-login']").serialize(),
                  success: function(data){
                      if(data){
                          //alert('Olá, bem-vindo!');
                          window.location.reload();
                      } else {
                          alert('E-mail ou senha incorretos.');
                      }
                  }

              });

          });
      });
  </script>

  <!-- Icons -->
  <?php echo file_get_contents("assets/symbols/svg/symbols.svg"); ?>
  

  <?php if (strstr($_SERVER['HTTP_HOST'],'localhost')): ?>
  <!-- Este script deve ser retirado em produção -->
  <script>
    document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
    ':35729/livereload.js?snipver=1"></' + 'script>')
  </script>
  <?php endif ?>

</body>
</html>