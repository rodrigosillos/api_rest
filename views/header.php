<?php //print_r($user_data_logged); ?>
<header id="header">
  <div class="header-content-area">
    <div class="container">
      <div class="row">
        <div class="col col-xs-12 has-logo">
          <a href="/" class="logo"><img src="assets/svg/logotipo.svg" alt="Supera - Sinergia em Saúde"></a>

          <button class="button is-link button-menu"><i class="fa fa-bars"></i></button>
        </div>
        <div class="col col-xs-12 has-form">
            <?php
            if(!isset($user_data_logged)) {
                include_once "components/form-login.php";
            }
            else
            {
                include_once "components/box-logged.php";
            }
            ?>
        </div>
      </div>
    </div>
  </div>

  <div class="header-menu-area">
    <div class="container">
      <nav class="nav is-feature">
        <ul>
          <li>
            <a href="/area-medica">
              <svg class="icon icon-estestoscopio"><use xlink:href="#icon-estestoscopio"></use></svg>
              <span>Área Médica</span>
            </a>
          </li>
          <li>
            <a href="/farmacia">
              <svg class="icon icon-quimica"><use xlink:href="#icon-quimica"></use></svg>
              <span>Farmácia</span>
            </a>
          </li>
          <li>
            <a href="/produtos">
              <svg class="icon icon-check-list"><use xlink:href="#icon-check-list"></use></svg>
              <span>Produtos</span>
            </a>
          </li>
          <li class="is-invert">
            <a href="/aplicativos">
              <svg class="icon icon-click-mobile"><use xlink:href="#icon-click-mobile"></use></svg>
              <span>Aplicativos</span>
            </a>
          </li>
        </ul> 
      </nav>
      <img src="assets/images/marcas.png" alt="Marcas - Supera">
    </div>
  </div>

</header>

<nav id="main-menu" class="nav is-primary">
  <div class="container">
    <ul>
      <li><a href="/quem-somos">Quem Somos</a></li>
      <li><a href="/dicas-de-saude">Dicas de Saúde</a></li>
      <li><a href="/fale-conosco">Fale Conosco</a></li>
      <li class="open-search"><span>Pesquisar <i class="fa fa-search"></i></span></li>
    </ul>
  </div> 
  <?php include "components/form-search.php";?>
</nav>

<div id="main-menu-mobile">
  <div class="content">
    <div class="row">
      <div class="col-4">
        <button class="close button is-link"><i class="fa fa-angle-left"></i> <span>Voltar</span></button>
        <ul>
          <li>
            <a href="/area-medica">
              <svg class="icon icon-estestoscopio"><use xlink:href="#icon-estestoscopio"></use></svg>
              <span>Área Médica</span>
            </a>
          </li>
          <li>
            <a href="/farmacia">
              <svg class="icon icon-quimica"><use xlink:href="#icon-quimica"></use></svg>
              <span>Farmácia</span>
            </a>
          </li>
          <li>
            <a href="/produtos">
              <svg class="icon icon-check-list"><use xlink:href="#icon-check-list"></use></svg>
              <span>Produtos</span>
            </a>
          </li>
          <li>
            <a href="/aplicativos">
              <svg class="icon icon-click-mobile"><use xlink:href="#icon-click-mobile"></use></svg>
              <span>Aplicativos</span>
            </a>
          </li>
        </ul> 
      </div>
      <div class="col-8">
        <?php include "components/form-search.php";?>

        <ul>
          <li><a href="/quem-somos">Quem Somos</a></li>
          <li><a href="/dicas-de-saude">Dicas de Saúde</a></li>
          <li><a href="/fale-conosco">Fale Conosco</a></li>
        </ul>

        <button class="button is-primary">Entrar / Cadastrar</button>

      </div>
    </div>
  </div>
</div>