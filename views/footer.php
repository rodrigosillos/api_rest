<footer id="footer">
  <div class="footer-wigets-area">
    <div class="container">
      <div class="row">
        <div class="col has-logo">
          <div class="widget">
            <img src="assets/svg/logotipo-white.svg" alt="SUPERA - Farma Laboratórios">
          </div>          
        </div>
        <div class="col">
          <div class="widget is-bordered">
            <h3 class="widget-title">Contato</h3>

            <ul>
              <li><i class="fa fa-phone"></i> PABX 11 5525.3200</li>
              <li><i class="fa fa-envelope"></i> supera.atende@superarx.com.br</li>
              <li>
                <a href="https://www.facebook.com/superarx" target="_blank"><i class="fa fa-facebook is-rounded"></i></a>
                <a href="https://br.linkedin.com/company/supera-farma" target="_blank"><i class="fa fa-linkedin is-rounded"></i></a>
              </li>
            </ul>
          </div>          
        </div>
        <div class="col">
          <div class="widget is-bordered">
            <h3 class="widget-title">Endereço</h3>

            <ul>
              <li><i class="fa fa-map-marker"></i> Supera</li>
              <li>
                <address>
                  Av. das Nações Unidas, 22.532 <br>
                  Jurubatuba <br>
                  04795-917 - São Paulo - SP
                </address>
              </li>
            </ul>
          </div>          
        </div>
      </div>
    </div>
  </div>

  <div class="copyright bg-secondary">
    <div class="container">
      <div class="row">
        <div class="col col-xs-12">
          SUPERA Farma Laboratórios S.A. © Todos os direitos reservados.        
        </div>
        <div class="col col-xs-12">
          <nav class="nav">
            <a href="/politica-de-privacidade">Política de Privacidade</a>
            <a href="/termos-de-uso">Termos de Uso</a>
            <a href="/trabalhe-conosco">Trabalhe Conosco</a>
          </nav>
        </div>
      </div>      
    </div>
  </div>

</footer> 