<div class="box-form is-login">
  <form action="" name="form-login" method="post">
    <div class="row">
      <div class="col">
        <div class="form-group">
          <input type="email" name="email" placeholder="E-mail" required>
        </div>
      </div>
      <div class="col">
        <div class="form-group">
          <input type="password" name="password" placeholder="Senha" required>
        </div>
      </div>
    </div>

    <div class="form-actions">
      <a href="#">Esqueci minha senha</a> ou <a href="#">Cadastre-se</a>
      <button type="submit" class="button is-submit">Entrar</button>
    </div>

  </form>
</div>