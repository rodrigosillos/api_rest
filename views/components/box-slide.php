<div class="box-slide slide-wrap">   
  <h2 class="box-slide-title">Novidades</h2>
  <div class="slide-controls">
    <button class="btn-prev prev-item"></button>
    <button class="btn-next next-item"></button>
  </div>

  <div class="slide" slide>

   <div class="item">
     <div class="row">
       <div class="col-sm-8 col-xs-12">
        <a href="#">
         <img src="assets/images/img-slide.jpg" alt="Novidades">
        </a>
       </div>
       <div class="col-sm-4 col-xs-12">
         <h2>40ª Lupa de Ouro</h2>
         <p>A Supera obteve grande destaque
          na 40ª edição do Lupa de Ouro 2016,
          o mais expressivo prêmio do marketing farmacêutico brasileiro.</p>

          <a href="#" class="button is-primary is-min-width is-right">Ler mais</a>
       </div>
     </div>
   </div>

   <div class="item">
     <div class="row">
       <div class="col-sm-8 col-xs-12">
        <a href="#">
         <img src="assets/images/img-slide.jpg" alt="Novidades">
        </a>
       </div>
       <div class="col-sm-4 col-xs-12">
         <h2>Título Slide 2</h2>
         <p>A Supera obteve grande destaque
          na 40ª edição do Lupa de Ouro 2016,
          o mais expressivo prêmio do marketing farmacêutico brasileiro.</p>

          <a href="#" class="button is-primary is-min-width is-right">Ler mais</a>
       </div>
     </div>
   </div>

  </div>

</div>