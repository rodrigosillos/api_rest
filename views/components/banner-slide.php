<div class="banner has-slide">
  <div class="slide" slide data-arrow>

    <div class="item">
      <div class="bg-cover bg" data-bg-desktop="assets/images/bg-banner.jpg" data-bg-mobile="assets/images/bg-banner.jpg"></div>
      <div class="container">
        <div class="banner-caption">
          <h3 class="banner-title">Sinergia em saúde</h3>
          <p>Sinergia entre a capacidade de inovação <br>e presença no consultório médico</p>
        </div>        
      </div>
    </div>

    <div class="item">
      <div class="bg-cover bg" data-bg-desktop="assets/images/bg-banner.jpg" data-bg-mobile="assets/images/bg-banner.jpg"></div>
      <div class="container">
        <div class="banner-caption">
          <h3 class="banner-title">Sinergia em saúde</h3>
          <p>Sinergia entre a capacidade de inovação <br>e presença no consultório médico</p>
        </div>        
      </div>
    </div>

  </div>
</div>