<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Product</title>
</head>
<body>

<div>
    <h1>Product</h1>
    <?php
    if($resultProduct)
    {
        foreach ($resultProduct as $product)
        {
            echo $product->name;
        }
    }
    ?>
</div>

</body>
</html>
