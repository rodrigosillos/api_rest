<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Partner extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('partner_model');
        $this->load->helper('uuid_helper');

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500;
        $this->methods['users_post']['limit'] = 100;
    }

    public function users_get()
    {
        $result = $this->partner_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $user){
                $resultCollection[] = [
                    'id' => $user->id,
                    'uuid' => $user->uuid,
                    'name' => $user->name,
                    'address' => $user->address,
                    'document' => $user->document,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function users_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->partner_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Partner already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'address' => $this->post('address'),
                'document' => $this->post('document'),
                'status' => $this->post('status'),
            ];

            $insert_id = $this->partner_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

}
