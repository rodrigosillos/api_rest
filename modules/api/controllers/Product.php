<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Product extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('product_model');
        $this->load->helper('uuid_helper');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500; // 500 requests per hour
        $this->methods['add_post']['limit'] = 100; // 100 requests per hour
    }

    public function list_get()
    {
        $result = $this->product_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $product){
                $resultCollection[] = [
                    'id' => $product->id,
                    'uuid' => $product->uuid,
                    'name' => $product->name,
                    'tags' => $product->tags,
                    'status' => $product->status,
                    'created_at' => $product->created_at,
                    'updated_at' => $product->updated_at,
                    'product_category_id' => $product->product_category_id,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->product_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Product already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'id' => $this->post('id'),
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'tags' => $this->post('tags'),
                'status' => $this->post('status'),
                'created_at' => date('Y-m-d H:i:s'),
                'product_category_id' => $this->post('category_id'),
            ];

            $insert_id = $this->product_model->set($data);

            $resultCollection = [
                'insert_id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_download_post()
    {
        $data = [
            'name' => $this->post('name'),
            'url' => $this->post('url'),
            'tags' => $this->post('tags'),
            'status' => $this->post('status'),
            'product_id' => $this->post('product_id'),
        ];

        $insert_id = $this->product_model->set_download($data);

        $resultCollection = [
            'insert_id' => $insert_id,
            'message' => 'Success'
        ];

        $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
    }

    public function add_description_post()
    {
        $data = [
            'name' => $this->post('name'),
            'text' => $this->post('text'),
            'product_id' => $this->post('product_id'),
        ];

        $insert_id = $this->product_model->set_description($data);

        $resultCollection = [
            'insert_id' => $insert_id,
            'message' => 'Success'
        ];

        $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
    }

    public function description_list_get()
    {
        $result = $this->product_model->get_description();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $description){
                $resultCollection[] = [
                    'id' => $description->id,
                    'name' => $description->name,
                    'text' => $description->text,
                    'product_id' => $description->product_id,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_image_post()
    {
        $data = [
            'name' => $this->post('name'),
            'position' => $this->post('position'),
            'url' => $this->post('url'),
            'status' => $this->post('status'),
            'product_id' => $this->post('product_id'),
        ];

        $insert_id = $this->product_model->set_image($data);

        $resultCollection = [
            'insert_id' => $insert_id,
            'message' => 'Success'
        ];

        $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
    }

    public function image_list_get()
    {
        $product_id = $this->get('product_id');

        if($product_id)
        {
            $data = [
                'product_id' => $product_id,
            ];

            $result = $this->product_model->get_image($data);

            if($result)
            {
                $resultCollection = null;

                foreach ($result as $image){
                    $resultCollection[] = [
                        'id' => $image->id,
                        'name' => $image->name,
                        'position' => $image->position,
                        'url' => $image->url,
                        'product_id' => $image->product_id,
                    ];
                }

                $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
            }
            else
            {
                $resultCollection = [
                    'message' => 'No records',
                ];

                $this->set_response($resultCollection, REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $resultCollection = [
                'message' => 'Product id is required',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

}
