<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Category extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('category_model');
        $this->load->helper('uuid_helper');

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500;
        $this->methods['add_post']['limit'] = 100;
    }

    public function list_get()
    {
        $result = $this->category_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $category){
                $resultCollection[] = [
                    'id' => $category->id,
                    'uuid' => $category->uuid,
                    'name' => $category->name,
                    'description' => $category->description,
                    'status' => $category->status,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->category_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Category already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'description' => $this->post('description'),
                'status' => $this->post('status'),
            ];

            $insert_id = $this->category_model->set($data);

            $resultCollection = [
                'insert_id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

}
