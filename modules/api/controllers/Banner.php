<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Banner extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('banner_model');
        $this->load->helper('uuid_helper');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500; // 500 requests per hour
        $this->methods['add_post']['limit'] = 100; // 100 requests per hour
    }

    public function list_get()
    {
        $result = $this->banner_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $banner){
                $resultCollection[] = [
                    'id' => $banner->id,
                    'uuid' => $banner->uuid,
                    'name' => $banner->name,
                    'position' => $banner->tags,
                    'url' => $banner->status,
                    'start_date' => $banner->start_date,
                    'end_date' => $banner->end_date,
                    'status' => $banner->status,
                    'created_at' => $banner->created_at,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->banner_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Banner name already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'position' => $this->post('position'),
                'url' => $this->post('url'),
                'start_date' => $this->post('start_date'),
                'end_date' => $this->post('end_date'),
                'status' => $this->post('status'),
                'created_at' => date('Y-m-d H:i:s'),
            ];

            $insert_id = $this->banner_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function edit_post()
    {
        $id = (int) $this->get('id');

        $check_data = [
            'id' => $id,
        ];

        $result = $this->banner_model->get($check_data);

        if($result)
        {
            $data = null;

            if($this->post('name'))
            {
                $data['name'] = $this->post('name');
            }
            if($this->post('position'))
            {
                $data['position'] = $this->post('position');
            }
            if($this->post('url'))
            {
                $data['url'] = $this->post('url');
            }
            if($this->post('start_date'))
            {
                $data['start_date'] = $this->post('start_date');
            }
            if($this->post('end_date'))
            {
                $data['end_date'] = $this->post('end_date');
            }
            if($this->post('status'))
            {
                $data['status'] = $this->post('status');
            }

            $this->banner_model->set($data, $check_data);

            $resultCollection = [
                'message' => 'Success',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = [
                'message' => 'Banner does not exist',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }
}
