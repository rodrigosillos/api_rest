<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Widget extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('widget_model');
        $this->load->helper('uuid_helper');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500; // 500 requests per hour
        $this->methods['add_post']['limit'] = 100; // 100 requests per hour
    }

    public function list_get()
    {
        $result = $this->widget_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $widget){
                $resultCollection[] = [
                    'id' => $widget->id,
                    'uuid' => $widget->uuid,
                    'name' => $widget->name,
                    'type' => $widget->type,
                    'status' => $widget->status,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->widget_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Widget name already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'type' => $this->post('type'),
                'status' => $this->post('status'),
            ];

            $insert_id = $this->widget_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }
}
