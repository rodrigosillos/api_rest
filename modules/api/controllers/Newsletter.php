<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Newsletter extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('newsletter_model');
        $this->load->helper('uuid_helper');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500; // 500 requests per hour
        $this->methods['add_post']['limit'] = 100; // 100 requests per hour
    }

    public function list_get()
    {
        $result = $this->newsletter_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $newsletter){
                $resultCollection[] = [
                    'id' => $newsletter->id,
                    'name' => $newsletter->name,
                    'email' => $newsletter->email,
                    'created_at' => $newsletter->created_at,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_post()
    {
        $check_data = [
            'email' => $this->post('email'),
        ];

        $result = $this->newsletter_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'E-mail already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'created_at' => date('Y-m-d H:i:s'),
            ];

            $insert_id = $this->newsletter_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }
}
