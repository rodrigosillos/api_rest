<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('uuid_helper');

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500;
        $this->methods['add_post']['limit'] = 100;
    }

    public function list_get()
    {
        $result = $this->user_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $user){
                $resultCollection[] = [
                    'id' => $user->id,
                    'uuid' => $user->uuid,
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => $user->password,
                    'auth' => $user->auth,
                    'status' => $user->status,
                    'created_at' => $user->created_at,
                    'user_profile_id' => $user->visibility,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function list_profile_get()
    {
        $result = $this->user_model->get_profile();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $profile){
                $resultCollection[] = [
                    'id' => $profile->id,
                    'name' => $profile->name,
                    'rules' => $profile->rules,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->user_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'User already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'email' => $this->post('email'),
                'password' => $this->post('password'),
                'date_birth' => $this->post('date_birth'),
                'gender' => $this->post('gender'),
                'document' => $this->post('document'),
                'occupation' => $this->post('occupation'),
                'institution' => $this->post('institution'),
                'state' => $this->post('state'),
                'city' => $this->post('city'),
                'expertise' => $this->post('expertise'),
                'phone' => $this->post('phone'),
                'status' => $this->post('status'),
                'created_at' => date('Y-m-d H:i:s'),
                'user_profile_id' => $this->post('user_profile_id'),
            ];

            $insert_id = $this->user_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_profile_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->user_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Profile already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'name' => $this->post('name'),
                'rules' => $this->post('rules'),
            ];

            $insert_id = $this->user_model->set_profile($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function auth_post()
    {
        $data = [
            'email' => $this->post('email'),
            'password' => $this->post('password'),
        ];

        $result = $this->user_model->get($data);

        if($result)
        {
            $resultCollection = [
                'logged' => true,
                'data' => $result,
                'message' => '',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = [
                'logged' => false,
                'data' => null,
                'message' => 'E-mail or password incorrect'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }
}
