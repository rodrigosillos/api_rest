<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Page extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('page_model');
        $this->load->helper(['uuid_helper','slug_helper']);

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500;
        $this->methods['add_post']['limit'] = 100;
    }

    public function list_get()
    {
        $result = $this->page_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $page){
                $resultCollection[] = [
                    'id' => $page->id,
                    'uuid' => $page->uuid,
                    'name' => $page->name,
                    'slug' => $page->slug,
                    'created_at' => $page->created_at,
                    'status' => $page->status,
                    'visibility' => $page->visibility,
                    'home' => $page->home,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->page_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Page name already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'slug' => gen_slug($this->post('name')),
                'created_at' => date('Y-m-d H:i:s'),
                'status' => $this->post('status'),
                'visibility' => $this->post('visibility'),
                'home' => $this->post('position'),
            ];

            $insert_id = $this->page_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

}
