<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Blog extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('blog_model');
        $this->load->helper(['uuid_helper','slug_helper']);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500; // 500 requests per hour
        $this->methods['add_post']['limit'] = 100; // 100 requests per hour
    }

    public function list_get()
    {
        $result = $this->blog_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $blog){
                $resultCollection[] = [
                    'id' => $blog->id,
                    'uuid' => $blog->uuid,
                    'title' => $blog->title,
                    'sub_title' => $blog->sub_title,
                    'text' => $blog->text,
                    'tags' => $blog->tags,
                    'slug' => $blog->slug,
                    'image_url' => $blog->image_url,
                    'status' => $blog->status,
                    'created_at' => $blog->created_at,
                    'category_id' => $blog->blog_category_id,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function add_post()
    {
        $check_data = [
            'title' => $this->post('title'),
        ];

        $result = $this->blog_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Blog title already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'title' => $this->post('title'),
                'sub_title' => $this->post('sub_title'),
                'text' => $this->post('text'),
                'tags' => $this->post('tags'),
                'slug' => gen_slug($this->post('title')),
                'image_url' => $this->post('image_url'),
                'status' => $this->post('status'),
                'created_at' => date('Y-m-d H:i:s'),
                'blog_category_id' => $this->post('blog_category_id'),
            ];

            $insert_id = $this->blog_model->set($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function edit_post()
    {
        $id = (int) $this->get('id');

        $check_data = [
            'id' => $id,
        ];

        $result = $this->banner_model->get($check_data);

        if($result)
        {
            $data = null;

            if($this->post('name'))
            {
                $data['name'] = $this->post('name');
            }
            if($this->post('position'))
            {
                $data['position'] = $this->post('position');
            }
            if($this->post('url'))
            {
                $data['url'] = $this->post('url');
            }
            if($this->post('start_date'))
            {
                $data['start_date'] = $this->post('start_date');
            }
            if($this->post('end_date'))
            {
                $data['end_date'] = $this->post('end_date');
            }
            if($this->post('status'))
            {
                $data['status'] = $this->post('status');
            }

            $this->banner_model->set($data, $check_data);

            $resultCollection = [
                'message' => 'Success',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = [
                'message' => 'Banner does not exist',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function add_category_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->blog_model->get_category($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Category name already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'uuid' => gen_uuid(),
                'name' => $this->post('name'),
                'status' => $this->post('status'),
            ];

            $insert_id = $this->blog_model->set_category($data);

            $resultCollection = [
                'id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function list_category_get()
    {
        $result = $this->blog_model->get_category();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $category){
                $resultCollection[] = [
                    'id' => $category->id,
                    'uuid' => $category->uuid,
                    'name' => $category->name,
                    'status' => $category->status,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }
}
