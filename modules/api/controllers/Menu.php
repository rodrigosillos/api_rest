<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Menu extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model');

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['list_get']['limit'] = 500;
        $this->methods['add_post']['limit'] = 100;
    }

    public function list_get()
    {
        $result = $this->menu_model->get();

        if(empty($result))
        {
            $resultCollection = [
                'message' => 'No records',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = null;

            foreach ($result as $menu){
                $resultCollection[] = [
                    'id' => $menu->id,
                    'name' => $menu->name,
                    'url' => $menu->url,
                    'image_url' => $menu->image_url,
                    'position' => $menu->position,
                    'status' => $menu->status,
                    'parent_id' => $menu->parent_id,
                    'page_id' => $menu->page_id,
                ];
            }

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

    public function add_post()
    {
        $check_data = [
            'name' => $this->post('name'),
        ];

        $result = $this->menu_model->get($check_data);

        if($result)
        {
            $resultCollection = [
                'message' => 'Menu already exists',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $data = [
                'name' => $this->post('name'),
                'url' => $this->post('url'),
                'image_url' => $this->post('image_url'),
                'position' => $this->post('position'),
                'status' => $this->post('status'),
                'parent_id' => $this->post('parent_id'),
                'page_id' => $this->post('page_id'),
            ];

            $insert_id = $this->menu_model->set($data);

            $resultCollection = [
                'insert_id' => $insert_id,
                'message' => 'Success'
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_CREATED);
        }
    }

    public function edit_post()
    {
        $id = (int) $this->get('id');

        $check_data = [
            'id' => $id,
        ];

        $result = $this->menu_model->get($check_data);

        if($result)
        {
            $data = null;

            if($this->post('name'))
            {
                $data['name'] = $this->post('name');
            }
            if($this->post('url'))
            {
                $data['url'] = $this->post('url');
            }
            if($this->post('image_url'))
            {
                $data['image_url'] = $this->post('image_url');
            }
            if($this->post('position'))
            {
                $data['position'] = $this->post('position');
            }
            if($this->post('status'))
            {
                $data['status'] = $this->post('status');
            }
            if($this->post('parent_id'))
            {
                $data['parent_id'] = $this->post('parent_id');
            }
            if($this->post('page_id'))
            {
                $data['page_id'] = $this->post('page_id');
            }

            $this->menu_model->set($data, $check_data);

            $resultCollection = [
                'message' => 'Success',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
        else
        {
            $resultCollection = [
                'message' => 'Menu does not exist',
            ];

            $this->set_response($resultCollection, REST_Controller::HTTP_OK);
        }
    }

}
