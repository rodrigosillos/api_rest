<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
    {
        $data_include = [
            'user_data_logged' => $this->user_data_logged,
        ];

        $data = [
            'user_data_logged' => $this->user_data_logged,
            'header' => $this->load->view('header', $data_include, TRUE),
            'content' => $this->load->view('home', $data_include, TRUE),
            'footer' => $this->load->view('footer', $data_include, TRUE),
        ];

        //print_r($this->user_data_logged);
        $this->load->view('template', $data);
    }
}
