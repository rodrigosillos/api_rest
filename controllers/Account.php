<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $this->load->view('account');
	}

	public function login()
    {
        $data = [
            'email' => $_POST['email'],
            'password' => $_POST['password'],
        ];

        $url = base_url() . '/api/user/auth';
        $resultApi = curl($url, 'POST', $data);
        $resultAuth = json_decode($resultApi);

        if($resultAuth->logged)
        {
            $this->session->set_userdata('user_data_logged', $resultAuth->data);
        }

        echo $resultAuth->logged;
    }

    public function logout()
    {
        $this->session->unset_userdata('user_data_logged');
        redirect(base_url());
    }

    public function register()
    {
        $data = [
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => $_POST['senha'],
            'date_birth' => $_POST['date_birth'],
            'gender' => $_POST['gender'],
            'document' => $_POST['document'],
            'occupation' => $_POST['occupation'],
            'institution' => $_POST['institution'],
            'state' => $_POST['state'],
            'city' => $_POST['city'],
            'expertise' => $_POST['expertise'],
            'phone' => $_POST['phone'],
            'user_profile_id' => $_POST['user_profile_id'],
        ];

        $url = base_url() . '/api/user/add';
        $resultApi = curl($url, 'POST', $data);
        $resultRegister = json_decode($resultApi);
    }
}
