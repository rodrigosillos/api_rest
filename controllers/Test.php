<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function pass_test()
	{
		//$this->load->view('test');
        $password = 'Acesso123#';
        $hash = password_hash ($password, PASSWORD_DEFAULT)."\n";

        //echo $hash;
        $hash_x = '$2y$10$6IKfvoojdhlXz//c3qTznerPw9bNMZSGxIgtUh.L9rGf05Aiu9xZq';
        echo password_verify ($password, $hash_x) ? 'true' : 'false';
	}

	public function index()
    {
        $this->load->view('test');
    }
}
