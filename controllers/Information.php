<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $this->load->view('information');
	}

	public function who_we_are()
    {
        $this->load->view('who_we_are');
    }

    public function health_tips()
    {
        $this->load->view('health_tips');
    }

    public function contact()
    {
        $this->load->view('contact');
    }
}
