<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
    }

	public function index()
	{
        $url = base_url() . '/api/product/list';
        $resultApi = curl($url);
        $resultProduct = json_decode($resultApi);

        $data = [
            'resultProduct' => $resultProduct,
        ];

        $this->load->view('product', $data);
	}

	public function detail()
    {
        $this->load->view('product_detail');
    }

    public function migrate()
    {
        $result = $this->product_model->get_migrate();

        foreach ($result as $migrate)
        {
            $data_product = [
                'id' => $migrate->ID,
                'name' => $migrate->post_title,
                'status' => true,
                'category_id' => 1,
            ];

            $url = base_url() . '/api/product/add';
            //$resultApi = curl($url, 'POST', $data_product);
            //print_r($resultApi);

            if($migrate->post_parent > 0)
            {
                $data_download = [
                    'name' => $migrate->post_title,
                    'url' => $migrate->guid,
                    'status' => true,
                    'product_id' => $migrate->post_parent,
                ];

                $url = base_url() . '/api/product/add_download';
                $resultApi = curl($url, 'POST', $data_download);
                //print_r($resultApi);
            }
        }
    }
}
