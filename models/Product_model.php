<?php

class Product_model extends CI_Model {
    
    public $table = "product";
    public $table_description = "product_description";
    public $table_image = "product_image";
    public $table_download = "product_download";
    public $table_migrate = "wp_products";
    
    function __construct()
    {
        parent::__construct();
    }

    function get_migrate($data=false)
    {
        $this->db->from($this->table_migrate);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        return $query->result();
    }
    
    function get($data=false)
    {
        $this->db->from($this->table);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        return $query->result();
    }
    
    function set($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table, $data);
        }
        else
        {
            $this->db->insert($this->table, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    function get_description($data=false)
    {
        $this->db->from($this->table_description);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        return $query->result();
    }

    function set_description($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table_description, $data);
        }
        else
        {
            $this->db->insert($this->table_description, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    function get_image($data=false)
    {
        $this->db->from($this->table_image);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        return $query->result();
    }

    function set_image($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table_image, $data);
        }
        else
        {
            $this->db->insert($this->table_image, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    function set_download($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table_download, $data);
        }
        else
        {
            $this->db->insert($this->table_download, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

}
