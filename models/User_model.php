<?php

class User_model extends CI_Model {
    
    public $table = "user";
    public $table_profile = "user_profile";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get($data=false)
    {
        $this->db->from($this->table);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }
    
    function set($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table, $data);
        }
        else
        {
            $this->db->insert($this->table, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    function get_profile($data=false)
    {
        $this->db->from($this->table_profile);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result();
    }

    function set_profile($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table_profile, $data);
        }
        else
        {
            $this->db->insert($this->table_profile, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
    
}
