<?php

class Newsletter_model extends CI_Model {
    
    public $table = "newsletter";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get($data=false)
    {
        $this->db->from($this->table);

        if($data)
        {
            $this->db->where($data);
        }

        $this->db->order_by('id', 'desc');

        $query = $this->db->get();
        return $query->result();
    }
    
    function set($data=false, $param=false)
    {
        if($param)
        {
            $this->db->where($param);
            $this->db->update($this->table, $data);
        }
        else
        {
            $this->db->insert($this->table, $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }
}
